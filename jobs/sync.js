 const cron = require("node-cron"),
       axios = require('axios');

const cronJob = () => {
    cron.schedule("0 * * * *", async() => {
        console.log(`one minute passed, image downloaded`);
        const result = await axios.get(process.env.URL_API + '/populate')
        console.log(result)
      });
}

module.exports = cronJob
