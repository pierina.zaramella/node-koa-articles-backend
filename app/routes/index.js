const Router = require('koa-router'),
     {articles, getArticlesById, setDeletedArticle} = require('../controllers/articles.controller'),
     setArticles = require('../controllers/sync.controller');

const router = new Router();

    router
        .get('/', async (ctx, next) => {
            ctx.body = "Server started!"
            await next()
        })
        .get('/articles', articles)
        .get('/articles/:id', getArticlesById)
        .put('/articles/:id', setDeletedArticle)
        .get('/populate', setArticles);

module.exports = {
    routes () { return router.routes() },
    allowedMethods () { return router.allowedMethods() }
};

