"use strict";

const db = require('../../data/articles.json'),
      fakeDelay=100;

module.exports = {

    /**
     * Get all records from memory DB
     * @return {Promise}
     */
    getAll: function getAllFromDb() {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(db || []);
            }, fakeDelay);
        });
    },

    /**
     * Get record by id from memory DB
     * @param id
     * @return {Promise}
     */
    getById: function getIdFromDb(id) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(db[parseInt(id)] || {});
            }, fakeDelay);
        });
    }
};
