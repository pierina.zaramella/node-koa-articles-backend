const axios = require('axios')

setArticles = async (ctx, next) => {
    const {data: { hits } } = await axios.get(process.env.URL_BLOG)

    for(let hit of hits){
        const {created_at, author, story_url} = hit
        await ctx.db.collection('blogs').findAndModify(
            {created_at, author, story_url},   // Query parameter
            { created_at: 1 },
            {...hit, deleted: false},
            { upsert: true } 
        )
    }
    ctx.body = 'Sync OK'
    await next();
}

module.exports = setArticles