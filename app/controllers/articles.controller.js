const ObjectId = require('mongodb').ObjectID, //TODO: change the implementation koa-mongo has issues with this ctx.mongo.ObjectID
     Joi = require('joi')

    // Simple user schema, more info: https://github.com/hapijs/joi
      userSchema = Joi.object().keys({
          id: Joi.string().trim().required()
      });
/**
 * @example curl -XGET "http://localhost:8081/articles/1"
 */
 const getArticlesById = async(ctx, next) => {
    await Joi.validate(ctx.params, userSchema, {allowUnknown: true});
    ctx.body = await ctx.db.collection('blogs').find({ _id: ObjectId(ctx.params.id) }).toArray();
    await next();
}

/**
* @example curl -XGET "http://localhost:8081/articles"
*/
 const articles = async(ctx, next) => {
    const list = await ctx.db.collection('blogs').find({deleted: false}).toArray();

    ctx.body = list.sort((a,b) => new Date(b.created_at) - new Date(a.created_at))
    
    await next();
 }

/**
* @example curl -XPUT "http://localhost:8081/articles/1"
*/
 const setDeletedArticle = async (ctx, next) => {
    const result = await ctx.db.collection('blogs').findAndModify(
        {_id: ObjectId(ctx.params.id)},   // Query parameter
        { created_at: 1 },
        {$set: {deleted: true}},
        { upsert: true } 
    )
    ctx.body = result
    await next();
 }

module.exports = {getArticlesById, articles, setDeletedArticle};