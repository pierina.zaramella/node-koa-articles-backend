require('dotenv').config();
const cron = require('../jobs/sync')
      http = require('http'),
      logger = require('koa-logger'),
      Koa = require('koa'),
      {app:configApp, server:configServer, database:configDB} = require('config'),
      err = require('./helpers/error'),
      {routes, allowedMethods}  = require('./routes'),
      mongo = require('koa-mongo'),
      setArticles = require('./controllers/sync.controller'),
      cors = require('koa-cors'),
      app = new Koa();

app.use(logger())
app.use(cors())
app.use(mongo(configDB))
app.use(err)
app.use(routes())
app.use(allowedMethods())

const server = http.createServer(app.callback()).listen(configServer.port, function () {
    console.log('%s listening at port %d', configApp.name, configServer.port);
    cron()
});

module.exports = {
    closeServer: () =>{
        server.close();
    },
    server
};