const request = require('supertest'),
      {server, closeServer} = require('../app/app');

// close the server after each test
afterAll(() => {
    closeServer()
    console.log('server closed!');
});
describe('basic route tests', () => {
    it('get home route GET /', async () => {
        const response = await request(server)
        setTimeout(() => {
            response.get('/');
            console.log(response)
            expect(response.status).toEqual(200);
            expect(response.text).toContain('Server started!');
        }, 1000)
        
    });
    it('get articles route GET /', async () => {
        const response = await request(server)
        setTimeout(() => {
            response.get('/articles');
            console.log(response)
            expect(response.status).toEqual(200);
        }, 1000)
        
    });
    it('get articles by ID GET /', async () => {
        const response = await request(server)
        setTimeout(() => {
            response.get('/articles/5e1ba84404fc57344c65e353');
            console.log(response)
            expect(response.status).toEqual(200);
        }, 1000)
    });
    it('put deleted article by ID GET /', async () => {
        const response = await request(server)
        setTimeout(() => {
            response.put('/articles/5e1ba84404fc57344c65e353');
            console.log(response)
            expect(response.status).toEqual(200);
        }, 1000)
        
    });
});