FROM node:latest as node-api

### copy modules
COPY package.json .
COPY app app
COPY config config
COPY data data
COPY jobs jobs
COPY __tests__ __tests__
COPY index.js .

### install dependencies
RUN npm install modclean -g
RUN npm install --silent
RUN modclean -r

# Please, check & delete unused files
FROM node:alpine
RUN mkdir /var/www
WORKDIR /var/www
COPY --from=node-api app app
COPY --from=node-api config config
COPY --from=node-api data data
COPY --from=node-api jobs jobs
COPY __tests__ __tests__
COPY --from=node-api index.js index.js
COPY --from=node-api node_modules node_modules
COPY --from=node-api package.json package.json

###set enviroments vars
ENV NODE_LIFE_TIME=ms
ENV NODE_APP_INSTANCE=3001
ENV MONGO_DB_USER=test
ENV MONGO_DB_PASS=Test.2020
ENV MONGO_DB_URI=cluster0-rk0e4.gcp.mongodb.net
ENV MONGO_DB_DB=blogs
ENV URL_BLOG=http://hn.algolia.com/api/v1/search_by_date?query=nodejs
ENV URL_API=http://localhost:3001

###start app
EXPOSE 3001
CMD node ./index.js