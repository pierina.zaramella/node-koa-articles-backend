let packages = require('../package');
module.exports = {
    app: {
        name: packages.name,
        version: packages.version
    },
    server: {
        port: process.env.NODE_APP_INSTANCE || 3001,
        lifeTime: process.env.NODE_LIFE_TIME || '', // For auto rebooting features use 'ms','m','s','h','d' suffix for this variable, for example 12h
    },
    database: {
        uri: `mongodb+srv://${process.env.MONGO_DB_USER}:${process.env.MONGO_DB_PASS}@${process.env.MONGO_DB_URI}/${process.env.MONGO_DB_DB}?retryWrites=true&w=majority`,
        max: 100,
        min: 1
    }
};